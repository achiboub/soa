package com.example.shop.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;


@Entity
@Table(name = "Livreur")
@JacksonXmlRootElement(localName = "LivreurXML")
public class Livreur {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JacksonXmlProperty(isAttribute = true)
	private Long idLiv ;
	
	@Column(name = "nom", nullable = false)
	@JacksonXmlProperty
	private String nom ;
	
	@Column(name = "prenom", nullable = false)
	@JacksonXmlProperty
    private String prenom; 
	
	@Column(name = "mail", nullable = false)
	@JacksonXmlProperty
    private String mail; 
	
	@Column(name = "password", nullable = false)
	@JacksonXmlProperty
    private String password;
	
	/*
	public List<Commande> getListCommande() {
		return listCommande;
	} */
/*
	public void setListCommande(List<Commande> listCommande) {
		this.listCommande = listCommande;
	}*/

	//@ManyToMany(mappedBy = "ListCommande")
	//public List<Commande> listCommande;
	

	public Livreur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getIdLiv() {
		return idLiv;
	}

	public void setIdLiv(Long idLiv) {
		this.idLiv = idLiv;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Livreur(Long idLiv, String nom, String prenom, String mail, String password) {
		super();
		this.idLiv = idLiv;
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.password = password;
	}

	
	
	
	
	
	
	

}
