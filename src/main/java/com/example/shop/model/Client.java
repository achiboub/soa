package com.example.shop.model;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name = "Client")
public class Client implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id ;
	
	@Column(name = "nom")
	private String nom ;
	
	@Column(name = "prenom")
    private String prenom; 
	
	@Column(name = "login")
    private String login; 
	
	@Column(name = "password")
    private String password; 
	
	@Column(name = "email")
    private String email; 
	
	@Column(name = "tel")
    private int tel; 
	
	@Column(name = "adress")
    private String adress; 
	
	
	
public Client() {

}
public Long getId() {
	return id;
}
public void setId(Long id) {
	this.id = id;
}
public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}
public String getPrenom() {
	return prenom;
}
public void setPrenom(String prenom) {
	this.prenom = prenom;
}

public String getLogin() {
	return login;
}
public void setLogin(String login) {
	this.login = login;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public int getTel() {
	return tel;
}
public void setTel(int tel) {
	this.tel = tel;
}
public String getAdress() {
	return adress;
}
public void setAdress(String adress) {
	this.adress = adress;
}



public Client(Long id, String nom, String prenom, String login, String password, String email, int tel, String adress) {
	super();
	this.id = id;
	this.nom = nom;
	this.prenom = prenom;
	this.login = login;
	this.password = password;
	this.email = email;
	this.tel = tel;
	this.adress = adress;
}





}
