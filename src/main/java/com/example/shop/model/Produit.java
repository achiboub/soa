package com.example.shop.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;



@Entity
@Table(name = "Produit")
//@XmlRootElement
public class Produit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long idProduit ;

	@Column(name = "Nom", nullable = true)
	private String nom;
    
	@Column(name = "Quantité", nullable = true)
	private String quantite;

	@Column(name = "Disponibilite")
	private String disponibilite;

	@Column(name = "motsCles", nullable = true)
	private String motsCles;

	@Column(name = "Image", nullable = true)
	private String path;

	@Column(name = "categorie", nullable = true)
	private String categorie;

	@JsonIgnore
	@ManyToMany(mappedBy = "ListProduit")
	public List<Commande> listCommande;

//	@XmlElement(name="nom")
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
//	@XmlElement(name="quantite")
	public String getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}
//	@XmlElement(name="disponibilite")
	
//	@XmlElement(name="motsCles")
	public String getMotsCles() {
		return motsCles;
	}

	public String getDisponibilite() {
		return disponibilite;
	}

	public void setDisponibilite(String disponibilite) {
		this.disponibilite = disponibilite;
	}

	public void setMotsCles(String motsCles) {
		this.motsCles = motsCles;
	}
	
//	@XmlElement(name="path")
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
//	@XmlElement(name="categorie")
	public String getCategorie() {
		return categorie;
	}
	
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	
//	@XmlElement(name="listCommande")
    @JsonIgnore
	public List<Commande> getListCommande() {
		return listCommande;
	}


//	@XmlElement(name="idProduit")
	public long getIdProduit() {
		return idProduit;
	}

	public void setIdProduit(long idProduit) {
		this.idProduit = idProduit;
	}

	public Produit(long idProduit, String nom, String quantite, String disponibilite, String motsCles, String path,
			String categorie) {
		super();
		this.idProduit = idProduit;
		this.nom = nom;
		this.quantite = quantite;
		this.disponibilite = disponibilite;
		this.motsCles = motsCles;
		this.path = path;
		this.categorie = categorie;
	}

	public Produit() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categorie == null) ? 0 : categorie.hashCode());
		result = prime * result + ((disponibilite == null) ? 0 : disponibilite.hashCode());
		result = prime * result + (int) (idProduit ^ (idProduit >>> 32));
		result = prime * result + ((listCommande == null) ? 0 : listCommande.hashCode());
		result = prime * result + ((motsCles == null) ? 0 : motsCles.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((quantite == null) ? 0 : quantite.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produit other = (Produit) obj;
		if (categorie == null) {
			if (other.categorie != null)
				return false;
		} else if (!categorie.equals(other.categorie))
			return false;
		if (disponibilite == null) {
			if (other.disponibilite != null)
				return false;
		} else if (!disponibilite.equals(other.disponibilite))
			return false;
		if (idProduit != other.idProduit)
			return false;
		if (listCommande == null) {
			if (other.listCommande != null)
				return false;
		} else if (!listCommande.equals(other.listCommande))
			return false;
		if (motsCles == null) {
			if (other.motsCles != null)
				return false;
		} else if (!motsCles.equals(other.motsCles))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (quantite == null) {
			if (other.quantite != null)
				return false;
		} else if (!quantite.equals(other.quantite))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Produit [idProduit=" + idProduit + ", nom=" + nom + ", quantite=" + quantite + ", disponibilite="
				+ disponibilite + ", motsCles=" + motsCles + ", path=" + path + ", categorie=" + categorie
				 +"]";
	}





}

