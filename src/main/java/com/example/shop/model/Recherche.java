package com.example.shop.model;

import java.util.List;

public class Recherche {
	
	private String mots;
	private List<Commande> listCommande; // charger par le controleur
	private List<Livreur> listLivreurs;
	private List<Produit> listProduit;
	private List<Client> listClients;
	private List<Categorie> listCategorie;
 	
	
	public List<Client> getListClients() {
		return listClients;
	}

	public void setListClients(List<Client> listClients) {
		this.listClients = listClients;
	}


	public List<Livreur> getListLivreurs() {
		return listLivreurs;
	}

	public void setListLivreurs(List<Livreur> listLivreurs) {
		this.listLivreurs = listLivreurs;
	}


	public List<Produit> getListProduit() {
		return listProduit;
	}

	public void setListProduit(List<Produit> listProduit) {
		this.listProduit = listProduit;
	}

	public List<Commande> getListCommande() {
		return listCommande;
	}

	public void setListCommande(List<Commande> listCommande) {
		this.listCommande = listCommande;
	}
	
	public List<Categorie> getListCategorie() {
		return listCategorie;
	}

	public void setListCategorie(List<Categorie> listCategorie) {
		this.listCategorie = listCategorie;
	}

	public Recherche() {
		super();
	}

	public String getMots() {
		return mots;
	}

	public void setMots(String mots) {
		this.mots = mots;
	}


	public Recherche(String mots, List<Commande> listCommande, List<Livreur> listLivreurs, List<Produit> listProduit,
			List<Client> listClients, List<Categorie> listCategorie) {
		super();
		this.mots = mots;
		this.listCommande = listCommande;
		this.listLivreurs = listLivreurs;
		this.listProduit = listProduit;
		this.listClients = listClients;
		this.listCategorie = listCategorie;
	}
}
