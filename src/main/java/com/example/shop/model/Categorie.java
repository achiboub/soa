package com.example.shop.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Categorie")
public class Categorie {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	 @JacksonXmlProperty
	private long idCategorie ;

	@Column(name = "nom")
	private String nom ;

	@JacksonXmlProperty
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "categorieProduit", 
	joinColumns = @JoinColumn(name = "idProduit"), 
	inverseJoinColumns = @JoinColumn(name = "idCategorie"))
	public List<Produit> ListProduit2 ;

	public List<Produit> getListProduit2() {
		return ListProduit2;
	}

	public void setListProduit2(List<Produit> listProduit2) {
		ListProduit2 = listProduit2;
	}


	public Categorie() {
		super();
	}


	public Categorie(long idCategorie, @NotEmpty(message = "nom est obligatoire") String nom, List<Produit> listProduit2) {
		super();
		this.idCategorie = idCategorie;
		this.nom = nom;
		ListProduit2 = listProduit2;
	}

	public long getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(long idCategorie) {
		this.idCategorie = idCategorie;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Categorie [idCategorie=" + idCategorie + ", nom=" + nom + " ListProduit2=" + ListProduit2 + "]";
	}

	


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ListProduit2 == null) ? 0 : ListProduit2.hashCode());
		result = prime * result + (int) (idCategorie ^ (idCategorie >>> 32));
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Categorie other = (Categorie) obj;
		if (ListProduit2 == null) {
			if (other.ListProduit2 != null)
				return false;
		} else if (!ListProduit2.equals(other.ListProduit2))
			return false;
		if (idCategorie != other.idCategorie)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}
}
