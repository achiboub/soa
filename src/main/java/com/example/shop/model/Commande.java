package com.example.shop.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Commande")
@JacksonXmlRootElement(localName = "UserXml")
public class Commande {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	 @JacksonXmlProperty
	private long idCommande ;
	

	@NotEmpty(message = "ref est obligatoire")
	@Column(name = "ref", nullable = false)
	 @JacksonXmlProperty
	private String ref ; 

	 @JacksonXmlProperty
	@NotEmpty(message = "adress est obligatoire")
	@Column(name = "adress", nullable = false)
	private String adress ;
	
	 @JacksonXmlProperty
	@NotEmpty(message = "codePostal est obligatoire")
	@Column(name = "codePostal", nullable = false)
	private int codePostal ;

	 @JacksonXmlProperty
	@NotEmpty(message = "idClient est obligatoire")
	@JoinColumn(name = "id_client", referencedColumnName = "id")
	@ManyToOne(optional = false)
	private Client id_client ;



	 @JacksonXmlProperty
	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinTable(name = "commandeProduit", 
	joinColumns = @JoinColumn(name = "idProduit"), 
	inverseJoinColumns = @JoinColumn(name = "idCommande"))
	public List<Produit> ListProduit ;

	public List<Produit> getListProduit() {
		return ListProduit;
	}


	public void setListProduit(List<Produit> listProduit) {
		ListProduit = listProduit;
	}


	public Commande() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Commande(long idCommande, @NotEmpty(message = "ref est obligatoire") String ref,
			@NotEmpty(message = "adress est obligatoire") String adress,
			@NotEmpty(message = "codePostal est obligatoire") int codePostal,
			@NotEmpty(message = "idClient est obligatoire") Client id_client, List<Produit> listProduit) {
		super();
		this.idCommande = idCommande;
		this.ref = ref;
		this.adress = adress;
		this.codePostal = codePostal;
		this.id_client = id_client;
		ListProduit = listProduit;
	}

	public String getFullName() {
		return id_client.getNom()+" "+id_client.getPrenom();
	}

	public long getIdCommande() {
		return idCommande;
	}


	public void setIdCommande(long idCommande) {
		this.idCommande = idCommande;
	}

	public String getRef() {
		return ref;
	}


	public void setRef(String ref) {
		this.ref = ref;
	}

	public String getAdress() {
		return adress;
	}


	public void setAdress(String adress) {
		this.adress = adress;
	}

	public int getCodePostal() {
		return codePostal;
	}


	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	public Client getId_client() {
		return id_client;
	}


	public void setId_client(Client id_client) {
		this.id_client = id_client;
	}


	@Override
	public String toString() {
		return "Commande [idCommande=" + idCommande + ", ref=" + ref + ", adress=" + adress + ", codePostal="
				+ codePostal + ", id_client=" + id_client + ", ListProduit=" + ListProduit + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ListProduit == null) ? 0 : ListProduit.hashCode());
		result = prime * result + ((adress == null) ? 0 : adress.hashCode());
		result = prime * result + codePostal;
		result = prime * result + (int) (idCommande ^ (idCommande >>> 32));
		result = prime * result + ((id_client == null) ? 0 : id_client.hashCode());
		result = prime * result + ((ref == null) ? 0 : ref.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Commande other = (Commande) obj;
		if (ListProduit == null) {
			if (other.ListProduit != null)
				return false;
		} else if (!ListProduit.equals(other.ListProduit))
			return false;
		if (adress == null) {
			if (other.adress != null)
				return false;
		} else if (!adress.equals(other.adress))
			return false;
		if (codePostal != other.codePostal)
			return false;
		if (idCommande != other.idCommande)
			return false;
		if (id_client == null) {
			if (other.id_client != null)
				return false;
		} else if (!id_client.equals(other.id_client))
			return false;
		if (ref == null) {
			if (other.ref != null)
				return false;
		} else if (!ref.equals(other.ref))
			return false;
		return true;
	}
}
