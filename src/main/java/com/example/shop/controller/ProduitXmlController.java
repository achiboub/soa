package com.example.shop.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.shop.exception.ResourceNotFoundException;
import com.example.shop.model.Produit;
import com.example.shop.repository.ProduitRepository;
import com.example.shop.service.ClientServiceImpl;
import com.example.shop.service.ProduitServiceImpl;



@RestController
public class ProduitXmlController {
	

	@Autowired
	private ProduitRepository ProduitRepo;

	@Autowired
	ProduitServiceImpl produitservice;
	
	@Autowired
	ClientServiceImpl clientservice;
	

	//// ****** xml Part *****

	@GetMapping(value = "/xml/produit/list", produces = MediaType.APPLICATION_XML)
	public List<Produit> getProduitsXml() {
		List<Produit> listcom = (List<Produit>) ProduitRepo.findAll();
		return listcom;
	}


	// JSON

	@GetMapping("/json/produits/list")
	public List<Produit> getAllProduits() {
		// http://localhost:8082/clinique/api/v1/produits
		return (List<Produit>) ProduitRepo.findAll();
	}

	//
	@PostMapping("/json/produits/add")
	public Produit createProduit(@Valid @RequestBody Produit produit) {
		return ProduitRepo.save(produit);
	}
	
	 @GetMapping("/json/produits/findByID/{id}")
	 public ResponseEntity<Produit> getProduitById(@PathVariable(value = "id") Long produitId)
	 throws com.example.shop.exception.ResourceNotFoundException { Produit produit = ProduitRepo.findById(produitId).orElseThrow(() -> new com.example.shop.exception.ResourceNotFoundException("Produit not found for this id :: " + produitId));
	 return ResponseEntity.ok().body(produit);
	 }
	
	 @PutMapping("/json/produits/update/{id}")
		public ResponseEntity<Produit> updateProduit(@PathVariable(value = "id") Long produitId,
				@Valid @RequestBody Produit produitDetails) throws ResourceNotFoundException {
			Produit produit = ProduitRepo.findById(produitId)
					.orElseThrow(() -> new ResourceNotFoundException("Produit not found for this id :: " + produitId));

			produit.setIdProduit(produitDetails.getIdProduit());
			produit.setNom(produitDetails.getNom());
			produit.setQuantite(produitDetails.getQuantite());
			produit.setCategorie(produitDetails.getCategorie());
			produit.setDisponibilite(produitDetails.getDisponibilite());
			produit.setPath(produitDetails.getPath());
			produit.setMotsCles(produitDetails.getMotsCles());

			final Produit updatedproduit = ProduitRepo.save(produit);
			return ResponseEntity.ok(updatedproduit);
		}	 

	 @DeleteMapping("/json/produits/delete/{id}")
	 public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id")
	 Long produitId)
	 throws ResourceNotFoundException { Produit produit = ProduitRepo.findById(produitId).orElseThrow(() -> new ResourceNotFoundException("Produit not found for this id :: " + produitId));
	
	 ProduitRepo.delete(produit);
	 Map<String, Boolean> response = new HashMap<>();
	 response.put("deleted", Boolean.TRUE);
	 return response;
	 }


}
