package com.example.shop.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

//@CrossOrigin(origins = "http://localhost:8082")
@RestController
//@RequestMapping("/api/v1")
public class PageController {
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() {
		ModelAndView model = new ModelAndView();
		model.setViewName("pages/home");
		
		return (model);
	}
	
	@RequestMapping(value = "/pages/about", method = RequestMethod.GET)
	public ModelAndView about() {
		ModelAndView model = new ModelAndView();
		model.setViewName("pages/about");
		
		return (model);
	}
}
