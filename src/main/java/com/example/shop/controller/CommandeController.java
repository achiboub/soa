package com.example.shop.controller;

import java.awt.PageAttributes.MediaType;
import java.util.HashMap;



import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.shop.model.Recherche;
import com.example.shop.model.Commande;
import com.example.shop.model.Produit;
import com.example.shop.repository.CommandeRepository;
import com.example.shop.service.CommandeServiceImpl;
import com.example.shop.service.ProduitService;

//@CrossOrigin(origins = "http://localhost:8082")
@RestController
//@RequestMapping("/api/v1")
public class CommandeController {

	@Autowired
	private CommandeRepository CommandeRepo;
	
	@Autowired
	CommandeServiceImpl Commandeservice;
	
	@Autowired
	ProduitService serviceProduit;
	
	@RequestMapping(value = "/commande/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView();
		/* récupérer la liste des Commandes à partir de la base */
		List<Commande> listcl =(List<Commande>) CommandeRepo.findAll();
		model.addObject("Commandes", listcl);
		model.setViewName("commande/index");
		
		return (model);
	}
	
	@RequestMapping(value = "/commande/add", method = RequestMethod.GET)
	public ModelAndView ajouter_client() {
		ModelAndView model = new ModelAndView();
		Commande a = new Commande(); // le modèle est une instance de la classe client
		model.addObject("CommandeForm", a); // ajout du modéle
		List<Produit>produits = serviceProduit.getAllProduits();
		model.addObject("produits", produits);
		model.setViewName("commande/create"); // indiquer le nom de la page html
		
		return (model);
	}
	
	@RequestMapping(value = "/commande/save", method = RequestMethod.POST)
	public ModelAndView saveUpdate(@ModelAttribute("CommandeForm") Commande c) {
		Commandeservice.saveOrUpdate(c);

		return (new ModelAndView("redirect:/commande/list"));
	}	
	
	@GetMapping("/commande/{id}")
	public ModelAndView getCommandeById(@PathVariable(value = "id") Long id)
	{
		ModelAndView model = new ModelAndView();
		Commande cl = Commandeservice.getCommandeById(id);
		List<Produit>produits = serviceProduit.getAllProduits();
		model.addObject("produits", produits);
		model.addObject("CommandeForm", cl);
		model.setViewName("commande/show");

		return model;
	}
	
	@RequestMapping(value = "/commande/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("id") long id) {
		ModelAndView model = new ModelAndView();
		Commande cl = Commandeservice.getCommandeById(id);
		List<Produit>produits = serviceProduit.getAllProduits();
		model.addObject("produits", produits);
		model.addObject("CommandeForm", cl);
		model.setViewName("commande/edit");

		return model;
	}

	@RequestMapping(value = "/commande/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") long id) {
				
		Commandeservice.deleteCommande(id);
	   return (new ModelAndView("redirect:/commande/list"));
	}
	
	@RequestMapping(value = "/commande/find", method = RequestMethod.GET)
	public ModelAndView find() {
		ModelAndView model = new ModelAndView();
		Recherche res = new Recherche(); // créer une instance de la classe Recherche
		model.addObject("resForm", res);
		// indiquer le nom de la page web à afficher
		model.setViewName("commande/search");
		return (model);
	}
	
	@RequestMapping(value = "/commande/recherche", method = RequestMethod.POST)
	public ModelAndView recherche(@ModelAttribute("resForm") Recherche mots) {
		ModelAndView model = new ModelAndView();
		/* récupérer les médecins correpondants à la spécialité sélectionnée */
		List<Commande> list = CommandeRepo.findByMots(mots.getMots());
		
		/* charger la liste de l'objet res ( instance de la classe de recherche ) */
		mots.setListCommande(list);
		
		model.addObject("resForm", mots);
		model.setViewName("commande/search");
		return (model);
	}
}
