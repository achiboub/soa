package com.example.shop.controller;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.servlet.ModelAndView;

//import com.example.shop.model.Recherche;
import com.example.shop.model.Client;
import com.example.shop.model.Commande;

import com.example.shop.repository.ClientRepository;
import com.example.shop.repository.CommandeRepository;
import com.example.shop.service.ClientServiceImpl;
import com.example.shop.exception.ResourceNotFoundException;

//import org.springframework.boot.context.config.ResourceNotFoundException;
//import org.springframework.web.bind.annotation.RequestParam;
//import com.example.shop.model.Livreur;
//import com.example.shop.repository.LivreurRepository;


//@CrossOrigin(origins = "http://localhost:8082")
@RestController
// @RequestMapping("/")
public class ClientXmlController {

	@Autowired
	private ClientRepository Client;

	@Autowired
	private CommandeRepository CommandeRepo;

	@Autowired
	ClientServiceImpl clientservice;

	//// ****** xml Part *****



	@GetMapping(value = "/xml/client/list", produces = MediaType.APPLICATION_XML)
	public List<Client> getCientsXml() {
		List<Client> listcl = (List<Client>) Client.findAll();
		return listcl;
	}

	// JSON

	@GetMapping("/json/clients/list")
	public List<Client> getAllEmployees() {
		// http://localhost:8082/clinique/api/v1/clients
		return (List<Client>) Client.findAll();
	}

	//
	@PostMapping("/json/clients/add")
	public Client createEmployee(@Valid @RequestBody Client client) {
		return Client.save(client);
	}
	
	 @GetMapping("/json/clients/findByID/{id}")
	 public ResponseEntity<Client> getEmployeeById(@PathVariable(value = "id") Long clientId)
	 throws com.example.shop.exception.ResourceNotFoundException { Client client = Client.findById(clientId).orElseThrow(() -> new com.example.shop.exception.ResourceNotFoundException("Client not found for this id :: " + clientId));
	 return ResponseEntity.ok().body(client);
	 }
	
	 @PutMapping("/json/clients/update/{id}")
		public ResponseEntity<Client> updateEmployee(@PathVariable(value = "id") Long clientId,
				@Valid @RequestBody Client clientDetails) throws ResourceNotFoundException {
			Client client = Client.findById(clientId)
					.orElseThrow(() -> new ResourceNotFoundException("Client not found for this id :: " + clientId));

			client.setId(clientDetails.getId());
			client.setNom(clientDetails.getNom());
			client.setPrenom(clientDetails.getPrenom());
			client.setLogin(clientDetails.getLogin());
			client.setPassword(clientDetails.getPassword());
			client.setEmail(clientDetails.getEmail());
			client.setTel(clientDetails.getTel());
			client.setAdress(clientDetails.getAdress());

			final Client updatedclient = Client.save(client);
			return ResponseEntity.ok(updatedclient);
		}	 
	
	 @DeleteMapping("/json/clients/delete/{id}")
	 public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id")
	 Long clientId)
	 throws ResourceNotFoundException { Client client = Client.findById(clientId).orElseThrow(() -> new ResourceNotFoundException("Client not found for this id :: " + clientId));
	
	 Client.delete(client);
	 Map<String, Boolean> response = new HashMap<>();
	 response.put("deleted", Boolean.TRUE);
	 return response;
	 }

	//
	// @PostMapping(path = "/Livreur/recherchel")
	// public List<Client> recherche(Recherche objectKey ,@RequestParam(value
	// ="mots") String mots)
	// throws Exception {
	// /* récupérer les liv correpondants role sélectionnée */
	// List<Client> listliv = null;
	// return listliv = Client.findByMots(objectKey.getMots());

	// //// ****** view Part *****
	//

}
