package com.example.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.shop.model.Produit;
import com.example.shop.model.Recherche;
import com.example.shop.service.ProduitService;

@RestController
public class ProduitController {
	@Autowired
	ProduitService service;

//URL : http://localhost:9090/produits
//	@GetMapping(value="/produitsxml", produces=MediaType.APPLICATION_XML)
//	public List<Produit> findProduits() {
//		List<Produit> list = service.getAllProduits();
//		return list;
//	}
	@GetMapping(value = "/produits")
	public List<Produit> findProduits() {
		List<Produit> list = service.getAllProduits();
		return list;
	}

	@RequestMapping(value = "/produits/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView();
		List<Produit> listcl = service.getAllProduits();

		model.addObject("listeproduits", listcl);
		model.setViewName("produits/index");
		return (model);

	}

	// http://localhost:9090/produit/{produitr-id}
	@GetMapping("/produit/{produit_id}")
	@ResponseBody
	public Produit produitById(@PathVariable("produit_id") long produitId) {
		return service.getProdutiById(produitId);
	}

	// http://localhost:9090/add-produit
//	@PostMapping("/add-produitxml")
//	@ResponseBody
//	@Consumes(MediaType.APPLICATION_XML)
//	public String addProduitxml(@RequestBody Produit produit) {
//		service.saveOrUpdate(produit);
//		return "produit ajouté";
//	}

	// http://localhost:9090/add-produit
//		@PostMapping("/add-produit")
//		@ResponseBody
	@RequestMapping(value = "/add-produit", method = RequestMethod.POST)
	public ModelAndView addProduit(@ModelAttribute Produit produit) {
		String path = "C:\\Users\\bechir\\Desktop\\soa\\" + produit.getPath();
		produit.setPath(path);
		service.saveOrUpdate(produit);
		return (new ModelAndView("redirect:/produits/list"));
	}

	@RequestMapping(value = "/produits/add", method = RequestMethod.GET)
	public ModelAndView ajouter_produit() {
		ModelAndView model = new ModelAndView();
		Produit produit = new Produit();
		model.addObject("produitForm", produit);
		model.setViewName("produits/create");
		return (model);

	}

	// http://localhost:9090/remove_produit/{produit_id}
//	@DeleteMapping("/remove_produit/{produit_id}")
//	@ResponseBody
//	public String  removeProduit(@PathVariable("produit_id") long produitId) {
//		service.deleteProduit(produitId);
//		return"produit dont le ID est "+ ""+produitId+"  "+"supprimé avec succés";
//	}
	@RequestMapping(value = "/remove_produit/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") long id) {
		service.deleteProduit(id);
		return new ModelAndView("redirect:/produits/list");
	}

	// http://localhost:9090/update_produit
	@PutMapping("/update_produit")
	@ResponseBody
	public String updateP(@RequestBody Produit produit) {

		service.saveOrUpdate(produit);
		return ("produit modifier");
	}

	@RequestMapping(value = "/produit/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("id") long id) {
		ModelAndView model = new ModelAndView();

		Produit cl = service.getProdutiById(id);
		model.addObject("produitForm", cl);
		model.setViewName("produits/edit");

		return model;
	}

	// http://localhost:9090/produitc/{categorie}}
	@GetMapping("/produitc/{categorie}")
	@ResponseBody
	public List<Produit> produitCategorie(@PathVariable("categorie") String categorie) {
		return service.findByCategorie(categorie);
	}

	// http://localhost:9090/produitm/{motsCles}}
	@GetMapping("/produitm/{motsCles}")
	@ResponseBody
	public List<Produit> produitMotsCles(@PathVariable("motsCles") String motsCles) {
		String mots = "%" + motsCles + "%";
		return service.findBymotsCles(mots);
	}

	// mots cles
	@RequestMapping(value = "/produit/find/mots", method = RequestMethod.GET)
	public ModelAndView findNp() {
		ModelAndView model = new ModelAndView();
		Recherche res = new Recherche();
		model.addObject("findForm", res);
		model.setViewName("produits/search");
		return (model);
	}

	// mots cles
	@RequestMapping(value = "/produit/recherche/mots", method = RequestMethod.POST)
	public ModelAndView rechercheNp(@ModelAttribute("findForm") Recherche res) {
		ModelAndView model = new ModelAndView();
		String mots = "%" + res.getMots() + "%";
		List<Produit> listProduit = service.findBymotsCles(mots);
		res.setListProduit(listProduit);
		model.addObject("findForm", res);
		model.setViewName("produits/search");
		return (model);
	}
	
	// categorie
	@RequestMapping(value = "/produit/find/categorie", method = RequestMethod.GET)
	public ModelAndView findcp() {
		ModelAndView model = new ModelAndView();
		Recherche res = new Recherche();
		model.addObject("findForm", res);
		model.setViewName("produits/searchByCategory");
		return (model);
	}

	// categorie
	@RequestMapping(value = "/produit/recherche/categorie", method = RequestMethod.POST)
	public ModelAndView recherchecp(@ModelAttribute("findForm") Recherche res) {
		ModelAndView model = new ModelAndView();
		List<Produit> listProduit = service.findByCategorie(res.getMots());
		res.setListProduit(listProduit);
		model.addObject("findForm", res);
		model.setViewName("produits/searchByCategory");
		return (model);
	}
}
