package com.example.shop.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.example.shop.model.Livreur;
import com.example.shop.model.Recherche;
import com.example.shop.repository.CommandeRepository;
import com.example.shop.repository.LivreurRepository;
import com.example.shop.service.LivreurService;

@RestController
public class LivreurTlController {
	
	@Autowired
	LivreurService livreurservice;
	
	@Autowired
	LivreurRepository agent ;
	
	@RequestMapping(value="/livreur/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView();
		//récupérer la liste des livreur à partir de la base 
		List<Livreur> listL= livreurservice.getAllLivreur();
		model.addObject("listelivreur", listL); 
		//indiquer le nom de la page html à afficher
		model.setViewName("livreurs/index");
		return (model);
	}
	
	@RequestMapping(value = "/livreur/add", method = RequestMethod.GET)
	public ModelAndView ajouter_livreur() {
		ModelAndView model = new ModelAndView();
		Livreur l = new Livreur();/// le modèle est une instance de la classe livreur
		model.addObject("LivreurForm", l);//// ajout du modèle 
		model.setViewName("livreurs/create");// indiquer le nom de la page html 
		return (model);
	}
	
	@RequestMapping(value = "/livreur/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("LivreurForm") Livreur l) {

		livreurservice.saveOrUpdate(l);
		//return("Livreur Ajouté");
		return (new ModelAndView("redirect:/livreur/list"));

	}
	
	@RequestMapping(value="/livreur/update/{id}", method= RequestMethod.GET)
	public ModelAndView update (@PathVariable("id") long id) {
		ModelAndView model = new ModelAndView() ; 
		/*récupérer le projet à modifier à partir de BD*/
		Livreur pro = livreurservice.getLivreurById(id); 
		model.addObject("LivreurForm", pro); 
		model.setViewName("livreurs/edit");
		return model;
	}
	
	@RequestMapping(value="/livreur/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") long id) {
		livreurservice.deleteLivreur(id);
		return (new ModelAndView("redirect:/livreur/list"));
	}
	
	@RequestMapping(value = "/livreur/find", method = RequestMethod.GET)
	public ModelAndView find() {
		ModelAndView model = new ModelAndView();
		Recherche res = new Recherche(); // créer une instance de la classe Recherche
		model.addObject("resForm", res);
		// indiquer le nom de la page web à afficher
		model.setViewName("livreurs/search");
		return (model);
	}
	
	@RequestMapping(value = "/livreur/recherche", method = RequestMethod.POST)
	public ModelAndView recherche(@ModelAttribute("resForm") Recherche mots) {
		ModelAndView model = new ModelAndView();
		/* récupérer les médecins correpondants à la spécialité sélectionnée */
		List<Livreur> list = agent.findByMots(mots.getMots());
		/* charger la liste de l'objet res ( instance de la classe de recherche ) */
		mots.setListLivreurs(list);
		model.addObject("resForm", mots);
		model.setViewName("livreurs/search");
		
		return (model);
	}
}
