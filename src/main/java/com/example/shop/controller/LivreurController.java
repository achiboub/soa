package com.example.shop.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.shop.exception.ResourceNotFoundException;
import com.example.shop.model.Livreur;
import com.example.shop.model.Recherche;
import com.example.shop.repository.LivreurRepository;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/rest")
public class LivreurController {

	@Autowired
	LivreurRepository agent ;
	
	
	/* show list */
	@GetMapping("/Livreur") 
	public List<Livreur> getAllLivreurs() {
		List<Livreur> listLivreurs = agent.findAll();
		//Sort.by("specialite").ascending()
		return (listLivreurs);
	}
	
	@GetMapping(value="/livreurxml", produces=MediaType.APPLICATION_XML)
	public List<Livreur> getAllLivreursXML() {
		List<Livreur> listLivreurs = agent.findAll();
		//Sort.by("specialite").ascending()
		return (listLivreurs);
	}
	
	/* show list xml
	@GetMapping("value="/Livreur", produces) 
	public List<Livreur> getAllLivreurs() {
		List<Livreur> listLivreurs = agent.findAll();
		//Sort.by("specialite").ascending()
		return (listLivreurs);
	}*/

	
	/* fonctionnalité d'ajout */
	@PostMapping("/livreur/add")
	public Livreur createLivreur(@Valid @RequestBody Livreur liv) {

		return agent.save(liv);
	}
	
	
	/* update */
	@PutMapping("/livreur/update/{id}")
	public ResponseEntity<Livreur> updateLivreur(@PathVariable(value = "id") Long livreurId, 
			@Valid @RequestBody Livreur livreurDetails) throws ResourceNotFoundException {
		Livreur liv = agent.findById(livreurId)
				.orElseThrow(() -> 
		new ResourceNotFoundException("Livreur not found for this id :: " + livreurId));
		liv.setIdLiv(livreurDetails.getIdLiv());
	    liv.setNom(livreurDetails.getNom());
	    liv.setPrenom(livreurDetails.getPrenom());
	    liv.setMail(livreurDetails.getMail());
	    liv.setPassword(livreurDetails.getPassword());
		final Livreur updatedliv = agent.save(liv);
		return ResponseEntity.ok(updatedliv);						
	}
	
	@GetMapping("/livreur/{id}")
	public ResponseEntity<Livreur> getLivreurById(@PathVariable(value = "id") long livreurId)
	throws ResourceNotFoundException{
		Livreur liv = agent.findById(livreurId)
				.orElseThrow(
						() -> new ResourceNotFoundException("Livreur not found for this id :: " + livreurId));
			return ResponseEntity.ok().body(liv);
						
	}
	
	@DeleteMapping("/livreur/delete/{id}")
	public Map<String, Boolean> deleteLivreur(@PathVariable(value ="id") Long id)
		throws ResourceNotFoundException {
		Livreur liv = agent.findById(id)
				.orElseThrow(
			() -> new ResourceNotFoundException("Livreur not found for" + "This id :: " + id));
		
		agent.delete(liv);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
