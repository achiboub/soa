package com.example.shop.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.shop.exception.ResourceNotFoundException;
import com.example.shop.model.Categorie;
import com.example.shop.repository.CategorieRepository;

import com.example.shop.service.CategorieServiceImpl;



@RestController
public class CategorieXmlController {
	

	@Autowired
	private CategorieRepository CategorieRepo;

	@Autowired
	CategorieServiceImpl categorieservice;
	

	

	//// ****** xml Part *****

	@GetMapping(value = "/xml/categorie/list", produces = MediaType.APPLICATION_XML)
	public List<Categorie> getCategoriesXml() {
		List<Categorie> listcom = (List<Categorie>) CategorieRepo.findAll();
		return listcom;
	}


	// JSON

	@GetMapping("/json/categories/list")
	public List<Categorie> getAllCategories() {
		// http://localhost:8082/clinique/api/v1/categories
		return (List<Categorie>) CategorieRepo.findAll();
	}

	//
	@PostMapping("/json/categories/add")
	public Categorie createCategorie(@Valid @RequestBody Categorie categorie) {
		return CategorieRepo.save(categorie);
	}
	
	 @GetMapping("/json/categories/findByID/{id}")
	 public ResponseEntity<Categorie> getCategorieById(@PathVariable(value = "id") Long categorieId)
	 throws com.example.shop.exception.ResourceNotFoundException { Categorie categorie = CategorieRepo.findById(categorieId).orElseThrow(() -> new com.example.shop.exception.ResourceNotFoundException("Categorie not found for this id :: " + categorieId));
	 return ResponseEntity.ok().body(categorie);
	 }
	
	 @PutMapping("/json/categories/update/{id}")
		public ResponseEntity<Categorie> updateCategorie(@PathVariable(value = "id") Long categorieId,
				@Valid @RequestBody Categorie categorieDetails) throws ResourceNotFoundException {
			Categorie categorie = CategorieRepo.findById(categorieId)
					.orElseThrow(() -> new ResourceNotFoundException("Categorie not found for this id :: " + categorieId));

			categorie.setIdCategorie(categorieDetails.getIdCategorie());
			categorie.setListProduit2(categorieDetails.getListProduit2());
			categorie.setNom(categorieDetails.getNom());
			


			final Categorie updatedcategorie = CategorieRepo.save(categorie);
			return ResponseEntity.ok(updatedcategorie);
		}	 

	 @DeleteMapping("/json/categories/delete/{id}")
	 public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id")
	 Long categorieId)
	 throws ResourceNotFoundException { Categorie categorie = CategorieRepo.findById(categorieId).orElseThrow(() -> new ResourceNotFoundException("Categorie not found for this id :: " + categorieId));
	
	 CategorieRepo.delete(categorie);
	 Map<String, Boolean> response = new HashMap<>();
	 response.put("deleted", Boolean.TRUE);
	 return response;
	 }


}
