package com.example.shop.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.shop.model.Recherche;
import com.example.shop.model.Categorie;
import com.example.shop.model.Produit;
import com.example.shop.repository.CategorieRepository;
import com.example.shop.service.CategorieServiceImpl;
import com.example.shop.service.ProduitService;

//@CrossOrigin(origins = "http://localhost:8082")
@RestController
//@RequestMapping("/api/v1")
public class CategorieController {

	@Autowired
	private CategorieRepository CategorieRepo;
	
	@Autowired
	CategorieServiceImpl Categorieservice;
	
	@Autowired
	ProduitService serviceProduit;
	
	@RequestMapping(value = "/categories/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView();
		/* récupérer la liste des Categories à partir de la base */
		List<Categorie> listcl =(List<Categorie>) CategorieRepo.findAll();
		model.addObject("Categories", listcl);
		model.setViewName("categories/index");
		
		return (model);
	}
	
	@RequestMapping(value = "/categories/create", method = RequestMethod.GET)
	public ModelAndView ajouter_client() {
		ModelAndView model = new ModelAndView();
		Categorie a = new Categorie(); // le modèle est une instance de la classe client
		model.addObject("CategorieForm", a); // ajout du modéle
		model.setViewName("categories/create"); // indiquer le nom de la page html
		
		return (model);
	}
	
	@RequestMapping(value = "/categories/save", method = RequestMethod.POST)
	public ModelAndView saveUpdate(@ModelAttribute("CategorieForm") Categorie c) {
		Categorieservice.saveOrUpdate(c);

		return (new ModelAndView("redirect:/categories/list"));
	}	
	
	@GetMapping("/categories/{id}")
	public ModelAndView getCategorieById(@PathVariable(value = "id") Long id)
	{
		ModelAndView model = new ModelAndView();
		Categorie cl = Categorieservice.getCategorieById(id);
		List<Produit>produits = serviceProduit.getAllProduits();
		model.addObject("produits", produits);
		model.addObject("CategorieForm", cl);
		model.setViewName("categories/show");

		return model;
	}
	
	@RequestMapping(value = "/categories/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("id") long id) {
		ModelAndView model = new ModelAndView();
		Categorie cl = Categorieservice.getCategorieById(id);
		model.addObject("CategorieForm", cl);
		model.setViewName("categories/edit");

		return model;
	}

	@RequestMapping(value = "/categories/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") long id) {
				
		Categorieservice.deleteCategorie(id);
	   return (new ModelAndView("redirect:/categories/list"));
	}
	
	@RequestMapping(value = "/categories/find", method = RequestMethod.GET)
	public ModelAndView find() {
		ModelAndView model = new ModelAndView();
		Recherche res = new Recherche(); // créer une instance de la classe Recherche
		model.addObject("resForm", res);
		// indiquer le nom de la page web à afficher
		model.setViewName("categories/search");
		return (model);
	}
	
	@RequestMapping(value = "/categories/recherche", method = RequestMethod.POST)
	public ModelAndView recherche(@ModelAttribute("resForm") Recherche mots) {
		ModelAndView model = new ModelAndView();
		/* récupérer les catégories correpondants à la spécialité sélectionnée */
		List<Categorie> list = CategorieRepo.findByMots(mots.getMots());
		
		/* charger la liste de l'objet res ( instance de la classe de recherche ) */
		mots.setListCategorie(list);
		
		model.addObject("resForm", mots);
		model.setViewName("categories/search");
		return (model);
	}
}
