package com.example.shop.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.shop.exception.ResourceNotFoundException;
import com.example.shop.model.Commande;
import com.example.shop.repository.CommandeRepository;
import com.example.shop.service.ClientServiceImpl;
import com.example.shop.service.CommandeServiceImpl;



@RestController
public class CommandeXmlController {
	

	@Autowired
	private CommandeRepository CommandeRepo;

	@Autowired
	CommandeServiceImpl commandeservice;
	
	@Autowired
	ClientServiceImpl clientservice;
	

	//// ****** xml Part *****

	@GetMapping(value = "/xml/commande/list", produces = MediaType.APPLICATION_XML)
	public List<Commande> getCommandesXml() {
		List<Commande> listcom = (List<Commande>) CommandeRepo.findAll();
		return listcom;
	}


	// JSON

	@GetMapping("/json/commandes/list")
	public List<Commande> getAllCommandes() {
		// http://localhost:8082/clinique/api/v1/commandes
		return (List<Commande>) CommandeRepo.findAll();
	}

	//
	@PostMapping("/json/commandes/add")
	public Commande createCommande(@Valid @RequestBody Commande commande) {
		return CommandeRepo.save(commande);
	}
	
	 @GetMapping("/json/commandes/findByID/{id}")
	 public ResponseEntity<Commande> getCommandeById(@PathVariable(value = "id") Long commandeId)
	 throws com.example.shop.exception.ResourceNotFoundException { Commande commande = CommandeRepo.findById(commandeId).orElseThrow(() -> new com.example.shop.exception.ResourceNotFoundException("Commande not found for this id :: " + commandeId));
	 return ResponseEntity.ok().body(commande);
	 }
	
	 @PutMapping("/json/commandes/update/{id}")
		public ResponseEntity<Commande> updateCommande(@PathVariable(value = "id") Long commandeId,
				@Valid @RequestBody Commande commandeDetails) throws ResourceNotFoundException {
			Commande commande = CommandeRepo.findById(commandeId)
					.orElseThrow(() -> new ResourceNotFoundException("Commande not found for this id :: " + commandeId));

			commande.setIdCommande(commandeDetails.getIdCommande());
			commande.setRef(commandeDetails.getRef());
			commande.setAdress(commandeDetails.getAdress());
			commande.setCodePostal(commandeDetails.getCodePostal());
			commande.setId_client(commandeDetails.getId_client());
			commande.setListProduit(commandeDetails.getListProduit());


			final Commande updatedcommande = CommandeRepo.save(commande);
			return ResponseEntity.ok(updatedcommande);
		}	 

	 @DeleteMapping("/json/commandes/delete/{id}")
	 public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id")
	 Long commandeId)
	 throws ResourceNotFoundException { Commande commande = CommandeRepo.findById(commandeId).orElseThrow(() -> new ResourceNotFoundException("Commande not found for this id :: " + commandeId));
	
	 CommandeRepo.delete(commande);
	 Map<String, Boolean> response = new HashMap<>();
	 response.put("deleted", Boolean.TRUE);
	 return response;
	 }


}
