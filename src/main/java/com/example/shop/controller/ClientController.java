package com.example.shop.controller;

import java.util.HashMap;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

//import com.example.shop.model.Recherche;
import com.example.shop.model.Client;
import com.example.shop.model.Commande;
import com.example.shop.model.Recherche;
import com.example.shop.repository.ClientRepository;
import com.example.shop.service.ClientServiceImpl;

//@CrossOrigin(origins = "http://localhost:8082")
@RestController
//@RequestMapping("/")
public class ClientController {

	@Autowired
	private ClientRepository Client;

	@Autowired
	ClientServiceImpl clientservice;

	@RequestMapping(value = "/clients/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView();

		/* récupérer la liste des clients à partir de la base */
		List<Client> listcl = (List<Client>) Client.findAll();
		model.addObject("clients", listcl);

		/* indiquer le nom de la page html à afficher */
		model.setViewName("clients/index");

		return (model);
	}
	
	@RequestMapping(value = "/clients/add", method = RequestMethod.GET)
	public ModelAndView ajouter_client() {
		ModelAndView model = new ModelAndView();
		Client a = new Client(); // le modèle est une instance de la classe client
		model.addObject("ClientForm", a); // ajout du modéle
		model.setViewName("clients/create"); // indiquer le nom de la page html
		return (model);

	}

	@RequestMapping(value = "/Clients/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("ClientForm") Client c) {
		clientservice.saveOrUpdate(c);
		return (new ModelAndView("redirect:/clients/list"));
		// return "le client est ajouté avec succées" +c;
	}

	@RequestMapping(value = "/clients/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("id") long id) {
		ModelAndView model = new ModelAndView();

		Client cl = clientservice.getClientById(id);
		model.addObject("ClientForm", cl);
		model.setViewName("clients/edit");

		return model;
	}

	@RequestMapping(value = "/Clients/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") long id) {
		clientservice.deleteClient(id);
		return (new ModelAndView("redirect:/clients/list"));
	}

	@RequestMapping(value = "/clients/find", method = RequestMethod.GET)
	public ModelAndView find() {
		ModelAndView model = new ModelAndView();
		Recherche res = new Recherche(); // créer une instance de la classe Recherche
		model.addObject("resForm", res);
		// indiquer le nom de la page web à afficher
		model.setViewName("clients/search");
		return (model);
	}

	@RequestMapping(value = "/clients/recherche", method = RequestMethod.POST)
	public ModelAndView recherche(@ModelAttribute("resForm") Recherche mots) {

		ModelAndView model = new ModelAndView();

		/* récupérer les médecins correpondants à la spécialité sélectionnée */

		List<Client> list = Client.findByMots(mots.getMots());

		/* charger la liste de l'objet res ( instance de la classe de recherche ) */
		mots.setListClients(list);
		model.addObject("resForm", mots);
		model.setViewName("clients/search");
		return (model);
	}

//	@GetMapping("/Clients/{id}")
//	public String getEmployeeById(@PathVariable(value = "id") Long id)
//			 {
//		Client Client = clientservice.getClientById(id);
//		return Client.toString();
//	}

//	@RequestMapping(value = "/clients/save", method = RequestMethod.POST)
//	public ModelAndView save(@ModelAttribute("clientForm") Client c) {
//
//		Client.save(c);
//
//		return (new ModelAndView("redirect:/clients/list"));
//
//	}

////****** xml  Part  *****	

//	@GetMapping(value="/Commandesxml",produces=MediaType.APPLICATION_XML)
//	public List<Commande> getCommandesXml()
//		 {
//		List<Commande> listcl =(List<Commande>)CommandeRepo.findAll();
//		return listcl;
//	}

	// JSON

//	@GetMapping("/clients")
//	public List<Client> getAllEmployees() {
//		// http://localhost:8082/clinique/api/v1/clients
//		return (List<Client>) Client.findAll();
//	}
//
//	@PostMapping("/clients")
//	public Client createEmployee(@Valid @RequestBody Client client) {
//		return Client.save(client);
//	}
//
//	@GetMapping("/clients/{id}")
//	public ResponseEntity<Client> getEmployeeById(@PathVariable(value = "id") Long clientId)
//			throws RessourceNotFoundException {
//		Client client = Client.findById(clientId)
//				.orElseThrow(() -> new RessourceNotFoundException("Client not found for this id :: " + clientId));
//		return ResponseEntity.ok().body(client);
//	}
//
//	@PutMapping("/clients/{id}")
//	public ResponseEntity<Client> updateEmployee(@PathVariable(value = "id") Long clientId,
//			@Valid @RequestBody Client clientDetails) throws RessourceNotFoundException {
//		Client client = Client.findById(clientId)
//				.orElseThrow(() -> new RessourceNotFoundException("Client not found for this id :: " + clientId));
//
//		client.setId(clientDetails.getId());
//		client.setNom(clientDetails.getNom());
//		client.setPrenom(clientDetails.getPrenom());
//		client.setLogin(clientDetails.getLogin());
//		client.setPassword(clientDetails.getPassword());
//		client.setEmail(clientDetails.getEmail());
//		client.setTel(clientDetails.getTel());
//		client.setAdress(clientDetails.getAdress());
//
//		final Client updatedclient = Client.save(client);
//		return ResponseEntity.ok(updatedclient);
//	}
//
//	@DeleteMapping("/clients/{id}")
//	public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long clientId)
//			throws RessourceNotFoundException {
//		Client client = Client.findById(clientId)
//				.orElseThrow(() -> new RessourceNotFoundException("Client not found for this id :: " + clientId));
//
//		Client.delete(client);
//		Map<String, Boolean> response = new HashMap<>();
//		response.put("deleted", Boolean.TRUE);
//		return response;
//	}

//	
//	@PostMapping(path = "/Livreur/recherchel")
//	public List<Client>  recherche(Recherche objectKey ,@RequestParam(value ="mots") String  mots)
//									 throws Exception {
//		/* récupérer les  liv correpondants role sélectionnée */
//		List<Client> listliv = null;
//		return	listliv = Client.findByMots(objectKey.getMots());

//	//// ****** view  Part  *****
//	

}
