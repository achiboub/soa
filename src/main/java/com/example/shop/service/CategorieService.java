package com.example.shop.service;

import java.util.List;

import com.example.shop.model.Categorie;

public interface CategorieService {
	
		 public List<Categorie> getAllCategories();
		 
		 public Categorie getCategorieById(long id);
		  
		 public void saveOrUpdate(Categorie Categorie);
		 
		 public void deleteCategorie(long id);
		 
		 public List<Categorie> findByMots(String mots);

}
