package com.example.shop.service;

import java.util.List;

import com.example.shop.model.Commande;

public interface CommandeService {
	
		 public List<Commande> getAllCommandes();
		 
		 public Commande getCommandeById(long id);
		  
		 public void saveOrUpdate(Commande Commande);
		 
		 public void deleteCommande(long id);
		 
		 public List<Commande> findByMots(String mots);

}
