package com.example.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.shop.model.Client;
//import com.example.shop.model.Commande;
import com.example.shop.repository.ClientRepository;



@Service
@Transactional

public class ClientServiceImpl implements ClientService {

	@Autowired // Automatic write + read
	ClientRepository client;

	@Override
	public List<Client> getAllClients() {
		// TODO Auto-generated method stub
		return (List<Client>) client.findAll();
	}

	@Override
	public Client getClientById(long id) {
		// TODO Auto-generated method stub
		return client.findById(id).get();
	}

	@Override
	public void saveOrUpdate(Client c) {
		// TODO Auto-generated method stub
		client.save(c);

	}

	@Override
	public void deleteClient(long id) {
		// TODO Auto-generated method stub
		client.deleteById(id);
	}

	@Override
	public List<Client> findByMots(String mots) {
		// TODO Auto-generated method stub
		return client.findByMots(mots);
	}

}
