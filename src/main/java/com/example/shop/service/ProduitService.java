package com.example.shop.service;

import java.util.List;

import com.example.shop.model.Produit;

public interface ProduitService {

	public List<Produit> getAllProduits();

	public Produit getProdutiById(long id);

	public void saveOrUpdate(Produit produit);

	public void deleteProduit(long id);

	public List<Produit> findBymotsCles(String mots);
	
	public List<Produit> findByCategorie(String categorie);

}
