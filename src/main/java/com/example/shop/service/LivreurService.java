package com.example.shop.service;

import java.util.List;


import com.example.shop.model.Livreur;

public interface LivreurService {
	
	public List<Livreur> getAllLivreur();
	 
	 public Livreur getLivreurById(long id);
	  
	 public void saveOrUpdate(Livreur Livreur);
	 
	 public void deleteLivreur(long id);
	 
	 public List<Livreur> findByMots(String mots);
}
