package com.example.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.shop.model.Produit;
import com.example.shop.repository.ProduitRepository;
@Service
@Transactional

public class ProduitServiceImpl implements ProduitService {

	@Autowired
	ProduitRepository repository;

	@Override
	public List<Produit> getAllProduits() {
		// TODO Auto-generated method stub
		return (List<Produit>) repository.findAll();
	}

	@Override
	public Produit getProdutiById(long id) {
		// TODO Auto-generated method stub
		return repository.findById(id).get();
	}

	@Override
	public void saveOrUpdate(Produit produit) {
		// TODO Auto-generated method stub
		repository.save(produit);

	}

	@Override
	public void deleteProduit(long id) {
		// TODO Auto-generated method stub
		repository.deleteById(id);

	}

	@Override
	public List<Produit> findBymotsCles(String mots) {
		// TODO Auto-generated method stub
		return (List<Produit>) repository.findBymotsCles(mots);
	}

	@Override
	public List<Produit> findByCategorie(String categorie) {
		// TODO Auto-generated method stub
		return repository.findByCategorie(categorie);
	}

}
