package com.example.shop.service;

import java.util.List;

import com.example.shop.model.Client;
//import com.example.shop.model.Commande;



public interface ClientService {

	public List<Client> getAllClients();
	 
	 public Client getClientById(long id);
	  
	 public void saveOrUpdate(Client client);
	 
	 public void deleteClient(long id);
	 
	 public List<Client> findByMots(String mots);
}
