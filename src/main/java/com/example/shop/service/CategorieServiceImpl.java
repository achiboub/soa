package com.example.shop.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.shop.model.Categorie;
import com.example.shop.repository.CategorieRepository;

@Service
@Transactional

public class CategorieServiceImpl implements CategorieService{
	
	@Autowired
	CategorieRepository Categorie;

	@Override
	public List<Categorie> getAllCategories() {
		// TODO Auto-generated method stub
		return (List<Categorie>)Categorie.findAll();
	}

	@Override
	public Categorie getCategorieById(long id) {
		// TODO Auto-generated method stub
		return Categorie.findById(id).get();
	}

	@Override
	public void saveOrUpdate(Categorie c) {
		// TODO Auto-generated method stub
		Categorie.save(c);
	}

	@Override
	public void deleteCategorie(long id) {
		// TODO Auto-generated method stub
		Categorie.deleteById(id);
	}

	@Override
	public List<Categorie> findByMots(String mots) {
		// TODO Auto-generated method stub
	return Categorie.findByMots(mots);
	
	}
	
}
