package com.example.shop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.example.shop.model.Livreur;
import com.example.shop.repository.LivreurRepository;


@Service
@Transactional
public class LivreurServiceImpl implements LivreurService {

	@Autowired
	LivreurRepository Livreur;
	
	@Override
	public List<Livreur> getAllLivreur() {
		// TODO Auto-generated method stub
		return (List<Livreur>)Livreur.findAll();
	}

	@Override
	public Livreur getLivreurById(long id) {
		// TODO Auto-generated method stub
		return Livreur.findById(id).get();
	}

	@Override
	public void saveOrUpdate(Livreur livreur) {
		// TODO Auto-generated method stub
		Livreur.save(livreur);
	}

	@Override
	public void deleteLivreur(long id) {
		// TODO Auto-generated method stub
		Livreur.deleteById(id);
	}

	@Override
	public List<Livreur> findByMots(String mots) {
		// TODO Auto-generated method stub
		
		return Livreur.findByMots(mots);
	}

}
