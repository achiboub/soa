package com.example.shop.service;

import java.util.List;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.shop.model.Commande;
import com.example.shop.repository.CommandeRepository;

@Service
@Transactional

public class CommandeServiceImpl implements CommandeService{
	
	@Autowired
	CommandeRepository Commande;

	@Override
	public List<Commande> getAllCommandes() {
		// TODO Auto-generated method stub
		return (List<Commande>)Commande.findAll();
	}

	@Override
	public Commande getCommandeById(long id) {
		// TODO Auto-generated method stub
		return Commande.findById(id).get();
	}

	@Override
	public void saveOrUpdate(Commande c) {
		// TODO Auto-generated method stub
		Commande.save(c);
	}

	@Override
	public void deleteCommande(long id) {
		// TODO Auto-generated method stub
		Commande.deleteById(id);
	}

	@Override
	public List<Commande> findByMots(String mots) {
		// TODO Auto-generated method stub
	return Commande.findByMots(mots);
	
	}
	
}
