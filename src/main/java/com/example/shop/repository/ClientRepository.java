package com.example.shop.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.shop.model.Client;

//import com.example.shop.model.Commande;
public interface ClientRepository extends JpaRepository<Client,Long> {
	@Query(value="Select m from Client m where m.nom Like %:mots%")
	public List<Client> findByMots(@Param("mots") String mots);
	
}
