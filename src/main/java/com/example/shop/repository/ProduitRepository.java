package com.example.shop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.shop.model.Commande;
import com.example.shop.model.Produit;

public interface ProduitRepository extends JpaRepository<Produit, Long> {

	public List<Produit> findByCategorie(String categorie);
	
	
	@Query(value="Select p from Produit p where p.motsCles Like :mots")
	public List<Produit> findBymotsCles(@Param("mots") String mots);
}
