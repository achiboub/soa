package com.example.shop.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.shop.model.Categorie;

public interface CategorieRepository extends JpaRepository<Categorie,Long> {
	@Query(value="Select m from Categorie m where m.nom Like :mots")
	public List<Categorie> findByMots(@Param("mots") String mots);
}
