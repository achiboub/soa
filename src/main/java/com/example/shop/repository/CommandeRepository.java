package com.example.shop.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.example.shop.model.Commande;

public interface CommandeRepository extends JpaRepository<Commande,Long> {
	@Query(value="Select m from Commande m where m.ref Like :mots")
	public List<Commande> findByMots(@Param("mots") String mots);
}
