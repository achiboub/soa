package com.example.shop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.example.shop.model.Livreur;

public interface LivreurRepository extends JpaRepository<Livreur,Long>{
	
	@Query(value="Select m from Livreur m where m.nom Like :mots")
	public List<Livreur> findByMots(@Param("mots") String mots);
	

}
